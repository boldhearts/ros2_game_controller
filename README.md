# ROS2 Robocup Game Controller

ROS 2 tools to work with the RoboCup Humanoid League Game
Controller[^1], consisting of the following packages:

## `game_controller_msgs`

The message interfaces used to communicate with the Game Controller,
of which the main ones are:

* `GameControlData` - the data broadcast by the Game Controller (see
  [wiki](https://github.com/RoboCup-Humanoid-TC/GameController/wiki/GameControlData)).
* `GameControlReturnData` - data returned by robots to announce their
  state to the Game Controller (see
  [wiki](https://raw.githubusercontent.com/wiki/RoboCup-Humanoid-TC/GameController/GameControlReturnData.md)).

## `game_controller`

Main package containing the `game_controller_node` that forms a bridge
between the Game Controller and ROS 2. Can be simply started with:

    ros2 run game_controller game_controller_node

#### Published Topics

* `/game_controller/control_data` - `game_controller_msgs/GameControlData`

    Control data broadcast by the Game Controller
    
#### Subscribed Topics

* `/game_controller/return_data` - `game_controller_msgs/GameControlReturnData`

    Data to be sent in return to the Game Controller. For instance, the following will notify that player one of team 3 is alive:
    
        ros2 topic pub /game_controller/return_data game_controller_msgs/GameControlReturnData '{team: 3, player: 1, message: 2}'

#### Parameters

* `game_controller_port` - `integer`, default: `3838`

    Destination port used by the Game Controller
    
* `game_controller_return_port` - `integer`, default: `3939`

    Destination port to send return data to
    


[^1]: Only the [Humanoid League
fork](https://github.com/RoboCup-Humanoid-TC/GameController) of the
Game Controller is currently supported, contributions to support the
Standard Platform League are welcomed.
