// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef GAME_CONTROLLER__BUFFER_WRITER_HPP_
#define GAME_CONTROLLER__BUFFER_WRITER_HPP_

#include <cstdint>
#include <type_traits>

namespace game_controller
{

class BufferWriter
{
public:
  explicit BufferWriter(char * ptr);

  template<typename T>
  void write(T val)
  {
    static_assert(std::is_integral<T>::value, "Integral type required");
    *reinterpret_cast<T *>(d_ptr) = val;
    d_ptr += sizeof(T);
  }

  std::size_t pos() const;

private:
  char const * const d_start;
  char * d_ptr;
};


inline BufferWriter::BufferWriter(char * ptr)
: d_start(ptr),
  d_ptr(ptr)
{}

inline std::size_t BufferWriter::pos() const
{
  return d_ptr - d_start;
}

}  // namespace game_controller

#endif  // GAME_CONTROLLER__BUFFER_WRITER_HPP_
