// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef GAME_CONTROLLER__BUFFER_READER_HPP_
#define GAME_CONTROLLER__BUFFER_READER_HPP_

#include <cstdint>
#include <type_traits>
#include <algorithm>

namespace game_controller
{

class BufferReader
{
public:
  explicit BufferReader(char const * ptr);

  template<typename T>
  T read()
  {
    static_assert(std::is_integral<T>::value, "Integral type required");
    auto val = *reinterpret_cast<const T *>(d_ptr);
    d_ptr += sizeof(T);
    return val;
  }

  template<typename T>
  void readBytes(T & buffer, std::size_t num)
  {
    std::copy(d_ptr, d_ptr + num, std::begin(buffer));
    d_ptr += num;
  }

  void skip(std::size_t num);

  std::size_t pos() const;

private:
  char const * const d_start;
  char const * d_ptr;
};

inline BufferReader::BufferReader(const char * ptr)
: d_start(ptr),
  d_ptr(ptr)
{}

inline void BufferReader::skip(std::size_t num)
{
  d_ptr += num;
}

inline std::size_t BufferReader::pos() const
{
  return d_ptr - d_start;
}

}  // namespace game_controller

#endif  // GAME_CONTROLLER__BUFFER_READER_HPP_
