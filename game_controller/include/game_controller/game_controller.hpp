// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef GAME_CONTROLLER__GAME_CONTROLLER_HPP_
#define GAME_CONTROLLER__GAME_CONTROLLER_HPP_

#include <netinet/in.h>
#include <rclcpp/rclcpp.hpp>
#include <memory>

#include "game_controller/visibility_control.h"
#include "game_controller/udp_socket.hpp"
#include "game_controller_msgs/msg/game_control_data.hpp"
#include "game_controller_msgs/msg/game_control_return_data.hpp"


namespace game_controller
{

class BufferReader;
class UDPSocket;

class GameController : public rclcpp::Node
{
public:
  GameController();

  virtual ~GameController();

private:
  static constexpr uint32_t GAME_STATE_MAGIC_NUMBER = 0x656d4752;  // "RGme"
  static constexpr uint32_t ROBOT_STATUS_MAGIC_NUMBER = 0x74724752;  // "RGrt"

  using GameControlData = game_controller_msgs::msg::GameControlData;
  using GameControlReturnData = game_controller_msgs::msg::GameControlReturnData;

  UDPSocket socket_;
  UDPSocket socket_out_;

  int game_controller_port_;
  int game_controller_return_port_;

  sockaddr_in game_controller_addr_;

  std::shared_ptr<rclcpp::TimerBase> receiveTimer_;

  rclcpp::Publisher<GameControlData>::SharedPtr gameControlDataPub_;

  rclcpp::Subscription<GameControlReturnData>::SharedPtr gameControlReturnDataSub_;

  GameControlData receive();
  GameControlData parse(BufferReader & reader);

  void send(GameControlReturnData & data);
};

}  // namespace game_controller

#endif  // GAME_CONTROLLER__GAME_CONTROLLER_HPP_
