// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "game_controller/game_controller.hpp"

#include <netinet/in.h>
#include <string>
#include "game_controller/buffer_reader.hpp"
#include "game_controller/buffer_writer.hpp"


using game_controller_msgs::msg::GameControlData;
using game_controller_msgs::msg::GameControlReturnData;
using game_controller_msgs::msg::TeamInfo;
using game_controller_msgs::msg::RobotInfo;

using namespace std::chrono_literals;

namespace game_controller
{

GameController::GameController()
: rclcpp::Node{"GameController"},
  game_controller_addr_{}
{
  gameControlDataPub_ = create_publisher<GameControlData>("/game_controller/control_data", 10);
  gameControlReturnDataSub_ = create_subscription<GameControlReturnData>(
    "/game_controller/return_data",
    10,
    [this](GameControlReturnData::SharedPtr data) {
      send(*data);
    });

  game_controller_port_ = int{};
  game_controller_return_port_ = int{};

  get_parameter_or("game_controller_port", game_controller_port_, 3838);
  get_parameter_or("game_controller_return_port", game_controller_return_port_, 3939);

  socket_.setBlocking(false);
  socket_.bind(game_controller_port_);

  socket_out_.setBlocking(false);
  socket_out_.bind(game_controller_return_port_ + 1000);

  RCLCPP_INFO(
    get_logger(),
    std::string{"GameControlData on UDP port "} + std::to_string(game_controller_port_));
  RCLCPP_INFO(
    get_logger(),
    std::string{"GameControlReturnData on UDP port "} + std::to_string(
      game_controller_return_port_));

  receiveTimer_ = create_wall_timer(
    100ms,
    [this]() {
      auto msg = receive();
      if (msg.gc_header != 0) {
        gameControlDataPub_->publish(msg);
      }
    });
}

GameController::~GameController()
{
}

GameControlData GameController::receive()
{
  static constexpr uint32_t MAX_MESSAGE_SIZE = 1023;

  char data[MAX_MESSAGE_SIZE + 1];
  auto fromAddress = sockaddr_in{};
  int fromAddressLength = sizeof(sockaddr_in);

  // Read message, allowing for one extra byte to be read
  // (so that messages which are too large can be ignored)
  int bytesRead = socket_.receiveFrom(data, MAX_MESSAGE_SIZE + 1, &fromAddress, &fromAddressLength);

  // Returns zero bytes when no message available (non-blocking)
  // Returns -1 when an error occurred. UDPSocket logs the error.
  if (bytesRead <= 0) {
    return GameControlData{};
  }

  game_controller_addr_ = fromAddress;

  auto reader = BufferReader{data};

  auto magicNumber = reader.read<uint32_t>();
  auto version = reader.read<uint16_t>();

  switch (magicNumber) {
    case GAME_STATE_MAGIC_NUMBER:
      {
        // Process game controller message
        RCLCPP_DEBUG(get_logger(), "Received game state message");
        auto msg = parse(reader);
        msg.gc_header = magicNumber;
        msg.protocol_version = version;
        return msg;
      }

    default:
      RCLCPP_ERROR(
        get_logger(),
        std::string{"Invalid magic number: "} + std::to_string(magicNumber));
      return GameControlData{};
  }
}

GameControlData GameController::parse(BufferReader & reader)
{
  auto msg = GameControlData{};

  msg.header.stamp = now();

  msg.packet_number = reader.read<uint8_t>();
  msg.players_per_team = reader.read<uint8_t>();
  msg.game_type = reader.read<uint8_t>();
  msg.state = reader.read<uint8_t>();
  msg.first_half = reader.read<uint8_t>();
  msg.kick_off_team = reader.read<uint8_t>();
  msg.secondary_state = reader.read<uint8_t>();
  msg.secondary_state_info = reader.read<int32_t>();
  msg.drop_in_team = reader.read<uint8_t>();
  msg.drop_in_time = reader.read<uint16_t>();
  msg.secs_remaining = reader.read<uint16_t>();
  msg.secondary_time = reader.read<uint16_t>();

  auto readRobot =
    [&]() -> RobotInfo {
      auto robot = RobotInfo{};

      robot.penalty = reader.read<uint8_t>();
      robot.secs_till_unpenalised = reader.read<uint8_t>();
      robot.yellow_card_count = reader.read<uint8_t>();
      robot.red_card_count = reader.read<uint8_t>();

      return robot;
    };

  auto readTeam =
    [&]() -> TeamInfo {
      auto team = TeamInfo{};

      team.team_number = reader.read<uint8_t>();
      team.team_colour = reader.read<uint8_t>();
      team.score = reader.read<uint8_t>();
      team.penalty_shot = reader.read<uint8_t>();
      team.single_shots = reader.read<uint16_t>();
      team.coach_sequence = reader.read<uint8_t>();
      reader.readBytes(team.coach_message, 253);

      team.coach = readRobot();
      for (auto i = 0u; i < team.players.size(); ++i) {
        team.players[i] = readRobot();
      }

      return team;
    };

  msg.teams[0] = readTeam();
  msg.teams[1] = readTeam();

  return msg;
}

void GameController::send(GameControlReturnData & data)
{
  // We need to have received a game controller message first, so we
  // know the target address to send return messages to
  if (game_controller_addr_.sin_family == 0) {
    RCLCPP_ERROR(get_logger(), "Trying to send return data before having received control data");
    return;
  }

  // Fill in default header and protocol version if not provided
  if (data.gc_header == 0) {
    data.gc_header = ROBOT_STATUS_MAGIC_NUMBER;
  } else if (data.gc_header != ROBOT_STATUS_MAGIC_NUMBER) {
    RCLCPP_INFO(get_logger(), "Return data header is not the expected default");
  }

  if (data.protocol_version == 0) {
    data.protocol_version = 2;
  }

  // Set the target to the last address we got a control message from
  game_controller_addr_.sin_port = htons(game_controller_return_port_);
  socket_out_.setTarget(game_controller_addr_);

  // Build packet
  char buffer[8];
  auto writer = BufferWriter{buffer};
  writer.write(data.gc_header);
  writer.write(data.protocol_version);
  writer.write(data.team);
  writer.write(data.player);
  writer.write(data.message);

  // Send it
  if (!socket_out_.send(buffer, 8)) {
    RCLCPP_ERROR(get_logger(), "Failed sending status response message to game controller");
  }
}

}  // namespace game_controller
