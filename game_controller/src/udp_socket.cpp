// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "game_controller/udp_socket.hpp"

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>

#include <cstdint>
#include <cstring>
#include <cerrno>

#include <string>

namespace game_controller
{

UDPSocket::UDPSocket()
: d_lastSendErrno(0)
{
  d_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  if (d_socket == -1) {
    RCLCPP_ERROR(
      get_logger(),
      std::string{"Unable to create datagram socket - (errono="} +
      std::to_string(errno) + " " + strerror(errno) + ")");
    throw std::runtime_error("Unable to create datagram socket");
  }

  d_target = new sockaddr_in;
}

UDPSocket::~UDPSocket()
{
  close(d_socket);
  delete d_target;
}

bool UDPSocket::setBlocking(bool isBlocking)
{
  int flags = fcntl(d_socket, F_GETFL, 0);

  if (flags < 0) {
    RCLCPP_ERROR(
      get_logger(),
      std::string{"Error in F_GETFL - (errono="} +
      std::to_string(errno) + " " + strerror(errno) + ")");
    return false;
  }

  if (isBlocking) {
    flags &= ~O_NONBLOCK;
  } else {
    flags |= O_NONBLOCK;
  }

  if (fcntl(d_socket, F_SETFL, flags) == -1) {
    RCLCPP_ERROR(
      get_logger(),
      std::string{"Error in F_SETFL "} +
      " - (errono=" + std::to_string(errno) + " " + strerror(errno) + ")");
    return false;
  }

  return true;
}

bool UDPSocket::setBroadcast(bool isBroadcast)
{
  int isBroadcastInt = isBroadcast ? 1 : 0;

  if (setsockopt(d_socket, SOL_SOCKET, SO_BROADCAST, &isBroadcastInt, sizeof(int))) {
    RCLCPP_ERROR(
      get_logger(),
      std::string{"Error setting socket option SO_BROADCAST to "} + std::to_string(isBroadcast) +
      " - (errono=" + std::to_string(errno) + " " + strerror(errno) + ")");
    return false;
  }

  return true;
}

bool UDPSocket::setMulticastLoopback(bool isLoopback)
{
  char isLoopbackChar = isLoopback ? 1 : 0;

  if (setsockopt(d_socket, IPPROTO_IP, IP_MULTICAST_LOOP, &isLoopbackChar, sizeof(char))) {
    RCLCPP_ERROR(
      get_logger(),
      std::string{"Error setting socket option IP_MULTICAST_LOOP to "} + std::to_string(
        isLoopback) +
      " - (errono=" + std::to_string(errno) + " " + strerror(errno) + ")");
    return false;
  }

  return true;
}

bool UDPSocket::setMulticastTTL(const u_char ttl)
{
  if (setsockopt(d_socket, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(u_char)) < 0) {
    RCLCPP_ERROR(
      get_logger(),
      std::string{"Error setting socket option IP_MULTICAST_TTL to "} + std::to_string(int{ttl}) +
      " - (errono=" + std::to_string(errno) + " " + strerror(errno) + ")");
    return false;
  }
  return true;
}

bool UDPSocket::setTarget(const sockaddr_in targetAddress)
{
  return memcpy(d_target, &targetAddress, sizeof(sockaddr_in));
}

bool UDPSocket::setTarget(std::string targetIpAddress, int port)
{
  return resolveIp4Address(targetIpAddress, port, static_cast<sockaddr_in *>(d_target));
}

bool UDPSocket::bind(int port)
{
  static const int one = 1;

  auto addr = sockaddr_in{};
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  addr.sin_port = htons((uint16_t)port);

  // Allow other sockets to bind() to this port, unless there is an active
  // listening socket bound to the port already. This gets around 'Address
  // already in use' errors.
  if (setsockopt(
      d_socket, SOL_SOCKET, SO_REUSEADDR, reinterpret_cast<const char *>(&one),
      sizeof(int)) == -1)
  {
    RCLCPP_ERROR(
      get_logger(),
      std::string{"Error setting socket option SO_REUSEADDR"} +
      " - (errono=" + std::to_string(errno) + " " + strerror(errno) + ")");

    // Continue, despite this error
  }

  if (::bind(d_socket, reinterpret_cast<sockaddr *>(&addr), sizeof(sockaddr_in)) == -1) {
    RCLCPP_ERROR(
      get_logger(),
      std::string{"Unable to bind address"} +
      " - (errono=" + std::to_string(errno) + " " + strerror(errno) + ")");
    return false;
  }

  // Obtain a textual name for the bound address, and print it out
  char boundAddressStr[INET_ADDRSTRLEN];
  inet_ntop(AF_INET, &(addr.sin_addr), boundAddressStr, INET_ADDRSTRLEN);

  RCLCPP_INFO(
    get_logger(),
    std::string{"Socket bound to "} + boundAddressStr + ":" + std::to_string(port));

  return true;
}

int UDPSocket::receive(char * data, int dataLength)
{
  return receiveFrom(data, dataLength, nullptr, nullptr);
}

int UDPSocket::receiveFrom(
  char * data, int dataLength, sockaddr_in * fromAddress,
  int * fromAddressLength)
{
  ssize_t bytesRead = recvfrom(
    d_socket, data, dataLength, 0,
    reinterpret_cast<sockaddr *>(fromAddress),
    reinterpret_cast<socklen_t *>(fromAddressLength));

  if (bytesRead < 0) {
    if (errno == EAGAIN) {
      // Response indicates that no data was available without blocking, so just return 0
      return 0;
    }

    RCLCPP_ERROR(
      get_logger(),
      std::string{"Error reading bytes"} +
      " - (errono=" + std::to_string(errno) + " " + strerror(errno) + ")");
    return -1;
  }

  return bytesRead;
}

bool UDPSocket::send(std::string const & message)
{
  return send(message.c_str(), message.length());
}

bool UDPSocket::send(const char * data, int dataLength)
{
  ssize_t bytesSent = sendto(
    d_socket, data, dataLength, 0,
    reinterpret_cast<sockaddr *>(d_target), sizeof(sockaddr));

  if (bytesSent < 0) {
    if (errno != d_lastSendErrno) {
      RCLCPP_ERROR(
        get_logger(),
        std::string{"Error sending bytes"} +
        " - (errono=" + std::to_string(errno) + " " + strerror(errno) + ")");
      d_lastSendErrno = errno;
    }
  } else if (d_lastSendErrno != 0) {
    d_lastSendErrno = 0;
  }

  return bytesSent > 0;
}

bool UDPSocket::resolveIp4Address(std::string const & ip4Address, int port, sockaddr_in * addr)
{
  memset(addr, 0, sizeof(sockaddr_in));

  addr->sin_family = AF_INET;
  addr->sin_port = htons((uint16_t)port);

  // Populate addr->sin_addr
  if (inet_pton(AF_INET, ip4Address.c_str(), &(addr->sin_addr.s_addr)) != 1) {
    RCLCPP_ERROR(
      get_logger(),
      std::string{"Unable to resolve IP4 address string: "} + ip4Address);
    return false;
  }

  return true;
}

}  // namespace game_controller
